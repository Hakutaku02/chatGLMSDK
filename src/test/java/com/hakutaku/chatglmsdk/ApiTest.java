package com.hakutaku.chatglmsdk;

import com.alibaba.fastjson.JSON;
import com.hakutaku.chatglmsdk.model.ChatCompletionAsyncResponse;
import com.hakutaku.chatglmsdk.model.ChatCompletionRequest;
import com.hakutaku.chatglmsdk.model.ChatCompletionResponse;
import com.hakutaku.chatglmsdk.model.enums.Model;
import com.hakutaku.chatglmsdk.model.enums.Role;
import com.hakutaku.chatglmsdk.session.ChatGlmSession;
import com.hakutaku.chatglmsdk.session.ChatGlmSessionFactory;
import com.hakutaku.chatglmsdk.session.Configuration;
import com.hakutaku.chatglmsdk.session.defaults.DefaultChatGlmSessionFactory;
import com.hakutaku.chatglmsdk.utils.BearerTokenUtils;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.sse.EventSource;
import okhttp3.sse.EventSourceListener;
import org.jetbrains.annotations.Nullable;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

@Slf4j
public class ApiTest {

    private ChatGlmSession chatGlmSession;

    @Before
    public void ChatGlmSessionFactory() {
        //  初始化配置
        Configuration configuration = new Configuration();
        configuration.setGlmApiHost("https://open.bigmodel.cn");
        configuration.setApiSecretKey("113f702606c24b65874f816ea716f321.QPlIBVRSatZfvcEI");
        configuration.setLevel(HttpLoggingInterceptor.Level.BODY);

        ChatGlmSessionFactory factory = new DefaultChatGlmSessionFactory(configuration);
        this.chatGlmSession = factory.chatGlmSession();
    }

    @Test
    public void testUrl(){
        Configuration configuration = new Configuration();
        configuration.setGlmApiHost("https://open.bigmodel.cn");
        configuration.setApiSecretKey("113f702606c24b65874f816ea716f321.QPlIBVRSatZfvcEI");

        String token = BearerTokenUtils.getToken(configuration.getApiKey(), configuration.getApiSecret());
        log.info("ApiKey is:{}",configuration.getApiKey());
        log.info("ApiSecret is:{}",configuration.getApiSecret());
        log.info("token is :{}",token);

    }

    @Test
    public void testCompletion4() throws Exception{
        CountDownLatch countDownLatch = new CountDownLatch(1);
        ChatCompletionRequest request = new ChatCompletionRequest();
        request.setModel(Model.GLM4_FLASH);
        request.setStream(true);

        request.setMessages(new ArrayList<ChatCompletionRequest.Prompt>(){
            public static final long serialVersionUID = -7988151926241837899L;
            {
                add(ChatCompletionRequest.Prompt.builder()
                        .role(Role.user.getRoleCode())
                        .content("1+1是多少？")
                        .build());
            }
        });

        chatGlmSession.completions(request, new EventSourceListener() {
            @Override
            public void onEvent(EventSource eventSource, @Nullable String id, @Nullable String type, String data) {
                if ("[DONE]".equals(data)){
                    log.info("[输出结束] Tokens {}", JSON.toJSONString(data));
                    return;
                }

                ChatCompletionResponse response = JSON.parseObject(data, ChatCompletionResponse.class);
                log.info("测试结果：{}", JSON.toJSONString(response));
            }

            @Override
            public void onClosed(EventSource eventSource) {
                log.info("对话完成");
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(EventSource eventSource, @Nullable Throwable t, @Nullable Response response) {
                log.error("对话失败", t);
                countDownLatch.countDown();
            }
        });
        countDownLatch.await();
    }

    @Test
    public void testCompletionSearch() throws Exception {
        CountDownLatch countDownLatch = new CountDownLatch(1);

        ChatCompletionRequest request = new ChatCompletionRequest();
        request.setModel(Model.GLM4_FLASH);
        request.setIncremental(false);

        request.setTools(new ArrayList<ChatCompletionRequest.Tool>(){
            public static final long serialVersionUID = -7988151926241837899L;

            {
                add((ChatCompletionRequest.Tool.builder()
                        .type(ChatCompletionRequest.Tool.Type.web_search))
                        .websearch(ChatCompletionRequest.Tool.WebSearch
                                .builder()
                                .enable(true)
                                .searchQuery("2025亚冬会")
                                .build())
                        .build());
            }
        });

        request.setPrompt(new ArrayList<ChatCompletionRequest.Prompt>(){
            public static final long serialVersionUID = -7988151926241837899L;

            {
                add(ChatCompletionRequest.Prompt.builder()
                        .role(Role.user.getRoleCode())
                        .content("2025亚冬会中国获得了多少奖牌")
                        .build());
            }
        });
        chatGlmSession.completions(request, new EventSourceListener() {
            @Override
            public void onEvent(EventSource eventSource, @Nullable String id, @Nullable String type, String data) {
                if ("[DONE]".equals(data)){
                    log.info("[输出结束] Tokens {}",
                            JSON.toJSONString(data));
                    return;
                }

                ChatCompletionResponse response = JSON.parseObject(data, ChatCompletionResponse.class);
                log.info("测试结果：{}", JSON.toJSONString(response));
            }

            @Override
            public void onClosed(EventSource eventSource) {
                log.info("对话完成");
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(EventSource eventSource,
                                  @Nullable Throwable t,
                                  @Nullable Response response) {
                log.error("对话失败", t);
                countDownLatch.countDown();
            }
        });

        countDownLatch.await();
    }

    //  流式对话样例
    @Test
    public void testCompletionFuture() throws Exception {

        ChatCompletionRequest request = new ChatCompletionRequest();
        request.setModel(Model.GLM4_FLASH);
        request.setPrompt(new ArrayList<ChatCompletionRequest.Prompt>(){
            public static final long serialVersionUID = -7988151926241837899L;

            {
                add(ChatCompletionRequest.Prompt.builder()
                        .role(Role.user.getRoleCode())
                        .content("1+1是多少？")
                        .build());
            }

        });

        ChatCompletionAsyncResponse response = chatGlmSession.completionsAsync(request);

        log.info("测试结果是：{}", JSON.toJSONString(response));
        System.out.println(response.getChoices()
                .get(0)
                .getMessage()
                .getContent());

    }

    @Test
    public void testCompletionFuture02() throws Exception {

        ChatCompletionRequest request = new ChatCompletionRequest();
        request.setModel(Model.GLM4_FLASH);
        request.setPrompt(new ArrayList<ChatCompletionRequest.Prompt>(){
            public static final long serialVersionUID = -7988151926241837899L;

            {
                add(ChatCompletionRequest.Prompt.builder()
                        .role(Role.user.getRoleCode())
                        .content("如何简便的计算任意两个两位数相乘？")
                        .build());
            }

        });

        ChatCompletionAsyncResponse response = chatGlmSession.completionsAsync(request);

        log.info("测试结果是：{}", JSON.toJSONString(response));
        System.out.println(response.getChoices()
                .get(0)
                .getMessage()
                .getContent());

    }

}
