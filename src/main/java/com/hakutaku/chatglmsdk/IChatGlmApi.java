/*
 * @Author: Hakutaku Hakutaku02@outlook.com
 * @Date: 2025-02-14 08:53:33
 * @LastEditors: Hakutaku Hakutaku02@outlook.com
 * @LastEditTime: 2025-02-17 10:54:57
 * @FilePath: \chatglmsdk\src\main\java\com\hakutaku\chatglmsdk\IChatGlmApi.java
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package com.hakutaku.chatglmsdk;

import com.hakutaku.chatglmsdk.model.ChatCompletionRequest;
import com.hakutaku.chatglmsdk.model.ChatCompletionResponse;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface IChatGlmApi {

    String v4Completions = "/api/paas/v4/chat/completions";
    String v4CompletionsAsync = "/api/paas/v4/async/chat/completions";
    String cogView3 = "/api/paas/v4/images/generations";

    //  同步调用CHATGLM接口
    @POST(v4Completions)
    Single<ChatCompletionResponse> completions(@Path("model")String model, @Body ChatCompletionRequest chatCompletionRequest);

    //  异步调用CHATGLM接口
    @POST(v4CompletionsAsync)
    Single<ChatCompletionResponse> completionsAsync(@Body ChatCompletionRequest chatCompletionRequest);

//    //  图像生成接口
//    @POST(cogView3)
//    Single<ImageCompletionResponse> generateImages(@Body ImageCompletionRequest imageCompletionRequest)
}
