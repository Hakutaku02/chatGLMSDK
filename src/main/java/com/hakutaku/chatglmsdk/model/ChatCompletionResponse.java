package com.hakutaku.chatglmsdk.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ChatCompletionResponse {

    /**
     * 一个成功的响应示例
     * {
     *   "created": 1703487403,
     *   "id": "8239375684858666781",
     *   "model": "glm-4-plus",
     *   "request_id": "8239375684858666781",
     *   "choices": [
     *       {
     *           "finish_reason": "stop",
     *           "index": 0,
     *           "message": {
     *               "content": "以AI绘蓝图 — 智谱AI，让创新的每一刻成为可能。",
     *               "role": "assistant"
     *           }
     *       }
     *   ],
     *   "usage": {
     *       "completion_tokens": 217,
     *       "prompt_tokens": 31,
     *       "total_tokens": 248
     *   }
     * }
     * 流式输出的响应体
     * data: {"id":"8313807536837492492","created":1706092316,"model":"glm-4-plus","choices":[{"index":0,"delta":{"role":"assistant","content":"土"}}]}
     * data: {"id":"8313807536837492492","created":1706092316,"model":"glm-4-plus","choices":[{"index":0,"delta":{"role":"assistant","content":"星"}}]}
     * ....
     * data: {"id":"8313807536837492492","created":1706092316,"model":"glm-4-plus","choices":[{"index":0,"delta":{"role":"assistant","content":"，"}}]}
     * data: {"id":"8313807536837492492","created":1706092316,"model":"glm-4-plus","choices":[{"index":0,"delta":{"role":"assistant","content":"主要由"}}]}
     * data: {"id":"8313807536837492492","created":1706092316,"model":"glm-4-plus","choices":[{"index":0,"finish_reason":"length","delta":{"role":"assistant","content":""}}],"usage":{"prompt_tokens":60,"completion_tokens":100,"total_tokens":160}}
     * data: [DONE]
     * 根据响应体编写实体类
     */

    //  GLM4模型的字段
    private String id;
    private Long created;
    private String model;
    private List<Choice> choices;
    private Usage usage;

    @JsonProperty("request_id")
    private String requestId;

    //  可能会用到的网络搜索字段，功能没开发到先按下不表
//    private WebResearch webResearch;

    //  编写成员的字段
    //  封装Choices
    @Data
    public static class Choice {
        //  结果索引
        private Long Index;
        //  终止原因
        @JsonProperty("finish_reason")
        private String finishReason;
        //  流式输出下的文本增量信息
        private Message message;
        //  可能调用的工具信息，因为没写到所以也按下不表
//        @JsonProperty("tool_calls")
//        private ToolCalls toolCalls;
    }

    @Data
    public static class Message {
        private String role;
        private String content;
    }

    //  封装Usage
    @Data
    public static class Usage{
        @JsonProperty("completion_tokens")
        private int completionTokens;

        @JsonProperty("prompt_tokens")
        private int promptTokens;

        @JsonProperty("total_tokens")
        private int totalTokens;
    }
}
