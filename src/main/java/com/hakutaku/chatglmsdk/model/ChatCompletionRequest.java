package com.hakutaku.chatglmsdk.model;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hakutaku.chatglmsdk.model.enums.Model;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Slf4j
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChatCompletionRequest {

    //  设置模型的默认参数
    private Boolean isCompatible = false;
    private Model model = Model.GLM4_FLASH;

    @JsonProperty("request_id")
    private String requestId = String.format("hakutaku-%d",System.currentTimeMillis());

    //  启用采用策略
    //  当do_sample为true时，启用采样策略；
    //  当do_sample为false时，温度和top_p等采样策略参数将不生效，模型输出随机性会大幅度降低。
    //  默认值为true。
    @JsonProperty("do_sample")
    private Boolean doSample = true;

    //  该参数在使用同步调用时应设置为false或省略,表示模型在生成所有内容后一次性返回所有内容。
    //  默认值为false。如果设置为true，模型将通过标准Event Stream逐块返回生成的内容。
    //  当Event Stream结束时，将返回一个data: [DONE]消息。
    @JsonProperty("stream")
    private Boolean stream = false;

    //  采样温度，控制输出的随机性，必须为正数 取值范围是：[0.0,1.0]， 默认值为 0.95
    //  值越大，会使输出更随机，更具创造性；值越小，输出会更加稳定或确定
    //  根据应用场景调整 top_p 或 temperature 参数，但不要同时调整两个参数
    @JsonProperty("temperature")
    private double temperature = 0.95d;

    //  用温度取样的另一种方法，称为核取样 取值范围是：[0.0, 1.0]，默认值为 0.70
    //  模型考虑具有 top_p 概率质量 tokens 的结果 例如：0.10 意味着模型解码器只考虑从前 10% 的概率的候选集中取 tokens
    //  根据应用场景调整 top_p 或 temperature 参数，但不要同时调整两个参数
    @JsonProperty("top_p")
    private double topP = 0.70d;

    //  控制生成的响应的最大 token 数量，
    //  默认值：动态计算（默认情况下，max_tokens的值会根据上下文长度减去输入长度来自动计算）
    //  最大值： max_tokens 最大支持4095，设置为超过 4095，则会被自动限制为 4095。
    @JsonProperty("max_tokens")
    private int maxTokens = 2048;

    //  模型遇到stop指定的字符时会停止生成。目前仅支持单个stop词，格式为["stop_word1"]。
    @JsonProperty("stop")
    private List<String> stop;

    //  模型可以调用的工具。
    private List<Tool> tools;

    //  用于控制模型是如何选择要调用的函数，仅当工具类型为function时补充。默认为auto，当前仅支持auto。
    //  24年1月发布的 GLM_3_5_TURBO、GLM_4 模型时新增
    @JsonProperty("tool_choice")
    private String toolChoice = "auto";

    //  会话内容
    //  消息类型的数据结构{"role":"user","content":"this is a message"}
    //  需要单独定义一个数据结构来初始化
    private List<Prompt> messages;

    //  输入给模型的会话信息
    //  用户输入角色就是user, 模型输出角色就是assistant
    private List<Prompt> prompt;

    private boolean incremental = true;

    //  sse增量模式okhttpsse截取data:问题，sseformat, [data: hello]。只在增量模式下使用sseFormat。
    private String sseFormat;

    //  会话内容的数据结构
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Prompt{
        private String role;
        private String content;

        public static PromptBuilder builder(){
            return new PromptBuilder();
        }

        public static class PromptBuilder{
            private String role;
            private String content;

            public PromptBuilder(){

            }
            public PromptBuilder role(String role){
                this.role = role;
                return this;
            }
            public PromptBuilder content(String content){
                this.content = content;
                return this;
            }

            public PromptBuilder content(Content content){
                this.content = JSON.toJSONString(content);
                return this;
            }

            public Prompt build(){
                return new Prompt(this.role,this.content);
            }

            public String toString(){
                return "ChatCompletionRequest.Prompt.PromptBuilder(role="
                        + this.role
                        + ", content="
                        + this.content + ")";
            }
        }
        @Data
        @Builder
        @NoArgsConstructor
        @AllArgsConstructor
        //  消息内容的数据结构，由文生图进行拓展
        public static class Content{
            private String type = Type.text.typeCode;
            private String text;
            @JsonProperty("image_url")
            private ImageUrl imageUrl;
            //  种类枚举常量
            @Getter
            @AllArgsConstructor
            public static enum Type{
                text("text","文本"),
                image_url("image_url","图"),
                ;
                private final String typeCode;
                private final String typeInfo;
            }

            @Data
            @Builder
            @NoArgsConstructor
            @AllArgsConstructor
            public static class ImageUrl{
                private String url;
            }
        }
    }

    //  工具内部类
    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Tool{
        //  工具类型，目前支持 function、retrieval、web_search。
        private Type type;
        private Function function;
        private Retrieval retrieval;
        @JsonProperty("web_search")
        private WebSearch websearch;

        public String getType(){return type.code;}

        @Getter
        @AllArgsConstructor
        public static enum Type{
            function("function","函数功能"),
            retrieval("retrieval","知识库"),
            web_search("web_search","联网"),
            ;
            private final String code;
            private final String info;
        }

        @Data
        @Builder
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Function{
            /**
             * name: `String` (必需): 函数名称，只能包含 a-z、A-Z、0-9、下划线和连字符。最大长度限制为64。
             * description: `String` (必需): 用于描述函数的能力。模型将根据此描述确定函数调用的方式。
             * parameters: `Object` (必需): 参数字段必须传递一个Json Schema对象，以准确定义函数接受的参数。如果调用函数时不需要参数，则可以省略此参数
             */
            private String name;
            private String description;
            private Object parameters;
        }

        @Data
        @Builder
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Retrieval{
            /**
             * 描述: 仅当工具类型为 retrieval 时补充。
             * knowledge_id: `String` (必需): 涉及知识库ID时，请前往开放平台的知识库模块创建或获取。
             * prompt_template: `String` (非必需): 请求模型时的知识库模板，默认模板：
             *                  ```从文档 "{{ knowledge }}" 中查找问题的答案 "{{question}}" 如果找到答案，仅使用文档的陈述来回答问题；如果未找到，则使用自己的知识回答，并告知用户此信息不是来自文档。不要重复问题，直接开始回答。```
             *                  用户自定义模板时，知识库内容占位符和用户端问题占位符必须分别为{{ knowledge }}和{{ question }};
             */
            @JsonProperty("knowledge_id")
            private String knowledgeId;

            //  默认知识库模板
            @JsonProperty("prompt_template")
            private String promptTemplate = """
                    \"\"\"
                    {{ knowledge}}
                    \"\"\"
                    中找问题
                    \"\"\"
                    {{question}}
                    \"\"\"
                    """;
        }
        @Data
        @Builder
        @NoArgsConstructor
        @AllArgsConstructor
        public static class WebSearch{
            // 是否启用搜索，默认启用搜索 enable = true/false
            private Boolean enable = true;
            //  强制搜索自定义关键内容，此时模型会根据自定义搜索关键内容返回的结果作为背景知识来回答用户发起的对话。
            @JsonProperty("search_query")
            private String searchQuery;
        }
    }

    //  拼接符合格式的Json字符串
    @Override
    public String toString(){
        try {
            Map<String,Object> paramsMap = new HashMap<>();
            paramsMap.put("model",this.model.getModelCode());
            if(null == this.messages && null == this.prompt){
                throw new RuntimeException("输入参数可能为空，请检查message与prompt参数");
            }
            paramsMap.put("messages",null != this.messages ? this.messages : this.prompt);
            if(null != this.requestId){
                paramsMap.put("request_id",this.requestId);
            }
            if (null != this.doSample){
                paramsMap.put("do_sample",this.doSample);
            }
            paramsMap.put("stream",this.stream);
            paramsMap.put("temperature",this.temperature);
            paramsMap.put("top_p",this.topP);
            paramsMap.put("max_tokens",this.maxTokens);
            if(null != this.stop && !this.stop.isEmpty()){
                paramsMap.put("stop",this.stop);
            }
            if (null != this.tools && !this.tools.isEmpty()){
                paramsMap.put("tools",this.tools);
                paramsMap.put("tool_choice",this.toolChoice);
            }
            return new ObjectMapper().writeValueAsString(paramsMap);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

    }
}
