package com.hakutaku.chatglmsdk.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ChatCompletionAsyncResponse {
    /**
     * 开发文档指定了两种接口的调用方式：同步调用与异步调用。
     * 同步调用中又含有正常的同步调用输出信息和同步调用的流式输出信息。
     * 刚刚编写的是同步响应和同步流式响应的响应实体类。
     * 现在需要编写异步响应的响应实体类
     */
    private String id;
    private String model;
    private List<Choice> choices;
    private Usage usage;

    @JsonProperty("request_id")
    private String requestId;
    @JsonProperty("task_status")
    private String taskStatus;

    @Data
    public static class Choice {
        private int index;
        private Message message;

        @JsonProperty("finish_reason")
        private String finishReason;
    }

    @Data
    public static class Usage {

        @JsonProperty("prompt_tokens")
        private int promptTokens;

        @JsonProperty("completion_tokens")
        private int completionTokens;

        @JsonProperty("total_tokens")
        private int totalTokens;
    }

    @Data
    public static class Message{
        private String role;
        private String content;
    }
}
