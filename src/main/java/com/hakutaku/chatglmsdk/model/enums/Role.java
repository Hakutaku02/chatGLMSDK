package com.hakutaku.chatglmsdk.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Role {

    user("user"),

    assistant("assistant"),

    system("system")
    ;

    private final String roleCode;
}
