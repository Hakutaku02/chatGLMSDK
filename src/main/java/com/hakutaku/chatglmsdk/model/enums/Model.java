package com.hakutaku.chatglmsdk.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Model {

    COGVIEW3_FLASH("cogview-3-flash", "根据用户的自然语言指令生成图像"),

    GLM4_PLUS("glm-4-plus","通常具备更强的计算能力和更大的模型规模，提供更精准和复杂的回答。"),

    GLM4_AIR("glm-4-air", "轻量化设计，运行效率高，资源消耗低。"),

    GLM4_LONG("glm-4-long","擅长处理长文本，能够理解和生成较长的内容。"),

    GLM4_FLASH("glm-4-flash","优化推理速度，提供超快响应")
    ;

    private final String modelCode;
    private final String modelInfo;
}
