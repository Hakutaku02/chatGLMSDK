package com.hakutaku.chatglmsdk.executor.glm;

import com.alibaba.fastjson.JSON;
import com.hakutaku.chatglmsdk.IChatGlmApi;
import com.hakutaku.chatglmsdk.executor.BaseExecutor;
import com.hakutaku.chatglmsdk.executor.handler.ResultHandler;
import com.hakutaku.chatglmsdk.model.ChatCompletionAsyncResponse;
import com.hakutaku.chatglmsdk.model.ChatCompletionRequest;
import com.hakutaku.chatglmsdk.model.ChatCompletionResponse;
import com.hakutaku.chatglmsdk.model.enums.EventType;
import com.hakutaku.chatglmsdk.session.Configuration;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import okhttp3.Request.Builder;
import okhttp3.sse.EventSource;
import okhttp3.sse.EventSourceListener;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Slf4j
public class GLMExecutor implements BaseExecutor, ResultHandler {

    //  加载配置
    private final Configuration config;
    private final EventSource.Factory eventSourceFactory;
    private final IChatGlmApi chatGlmApi;
    private final OkHttpClient okHttpClient;

    public GLMExecutor(Configuration config){
        this.config = config;
        this.eventSourceFactory = config.createRequestFactory();
        this.chatGlmApi = config.getChatGlmApi();
        this.okHttpClient = config.getOkHttpClient();
    }

    @Override
    public EventSource completions(ChatCompletionRequest chatCompletionRequest, EventSourceListener eventSourceListener) {
        Request request = new Builder()
                .url(config.getGlmApiHost().concat(IChatGlmApi.v4Completions))
                .post(RequestBody.create(MediaType.parse(
                        Configuration.JSON_CONTENT_TYPE),
                        chatCompletionRequest.toString()))
                .build();

        //  回传事件结果
        return eventSourceFactory.newEventSource(request,
                chatCompletionRequest.getIsCompatible()
                ? eventSourceListener(eventSourceListener) : eventSourceListener);
    }

    @Override
    public CompletableFuture<String> completions(ChatCompletionRequest chatCompletionRequest) {
        CompletableFuture<String> tmpFuture = new CompletableFuture<>();
        StringBuffer dataBuffer = new StringBuffer();

        //  构建请求
        Request request = new Builder()
                .url(config.getGlmApiHost().concat(IChatGlmApi.v4CompletionsAsync))
                .post(RequestBody.create(MediaType.parse("application/json;charset=utf-8")
                        ,chatCompletionRequest.toString()))
                .build();

        eventSourceFactory.newEventSource(request, new EventSourceListener() {
            @Override
            public void onEvent(EventSource eventSource, @Nullable String id, @Nullable String type, String data) {
                if ("[Done]".equals(data)){
                    log.info("[输出结束] Tokens {}", JSON.toJSONString(data));
                    return;
                }

                ChatCompletionResponse response = JSON.parseObject(data, ChatCompletionResponse.class);
                log.info("测试结果：{}", JSON.toJSONString(response));
                List<ChatCompletionResponse.Choice> choiceList = response.getChoices();
                for (ChatCompletionResponse.Choice choice : choiceList){
                    dataBuffer.append(choice.getMessage().getContent());
                }

            }

            @Override
            public void onClosed(EventSource eventSource) {
                tmpFuture.complete(dataBuffer.toString());
            }

            @Override
            public void onFailure(EventSource eventSource, @Nullable Throwable t, @Nullable Response response) {
                tmpFuture.completeExceptionally(new RuntimeException("Request closed before completion"));

            }
        });
        return tmpFuture;
    }
    @Override
    public ChatCompletionAsyncResponse completionsAsync(ChatCompletionRequest chatCompletionRequest) throws Exception {
        //  sync同步请求就不用了直接false
        chatCompletionRequest.setStream(false);

        //  构建请求
        Request request = new Builder()
                .url(config.getGlmApiHost().concat(IChatGlmApi.v4Completions))
                .post(RequestBody.create(MediaType.parse(Configuration.JSON_CONTENT_TYPE),
                         chatCompletionRequest.toString()))
                .build();
        //  构建客户端
        OkHttpClient okHttpClient = config.getOkHttpClient();
        Response response = okHttpClient.newCall(request).execute();
        if(!response.isSuccessful()){
            throw new RuntimeException("Request Failed");
        }
        return JSON.parseObject(response.body().string(),
                ChatCompletionAsyncResponse.class);
    }

    @Override
    public EventSourceListener eventSourceListener(
            EventSourceListener eventSourceListener){
        return new EventSourceListener() {
            @Override
            public void onEvent(EventSource eventSource, @Nullable String id, @Nullable String type, String data) {
                if ("[DONE]".equals(data)) {
                    return;
                }
                ChatCompletionResponse response = JSON.parseObject(data, ChatCompletionResponse.class);
                if (response.getChoices() != null && 1 == response.getChoices().size() && "stop".equals(response.getChoices().get(0).getFinishReason())) {
                    eventSourceListener.onEvent(eventSource, id, EventType.finish.getCode(), data);
                    return;
                }
                eventSourceListener.onEvent(eventSource, id, EventType.add.getCode(), data);
            }

            @Override
            public void onClosed(EventSource eventSource) {
                eventSourceListener.onClosed(eventSource);
            }

            @Override
            public void onFailure(EventSource eventSource, @Nullable Throwable t, @Nullable Response response) {
                eventSourceListener.onFailure(eventSource, t, response);
            }
        };
    }

}
