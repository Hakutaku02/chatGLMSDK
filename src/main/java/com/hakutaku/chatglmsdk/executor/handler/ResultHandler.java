package com.hakutaku.chatglmsdk.executor.handler;

import okhttp3.sse.EventSourceListener;

public interface ResultHandler {
    EventSourceListener eventSourceListener(
            EventSourceListener eventSourceListener);
}
