package com.hakutaku.chatglmsdk.executor;

import com.hakutaku.chatglmsdk.model.ChatCompletionAsyncResponse;
import com.hakutaku.chatglmsdk.model.ChatCompletionRequest;
import okhttp3.sse.EventSource;
import okhttp3.sse.EventSourceListener;

import java.util.concurrent.CompletableFuture;

public interface BaseExecutor {

    /**
     * 接收直接回答的信息
     * @param chatCompletionRequest
     * @param eventSourceListener
     * @return
     * @throws Exception
     */
    EventSource completions(ChatCompletionRequest chatCompletionRequest,
                                    EventSourceListener eventSourceListener)
    throws Exception;

    /**
     * 接受流式输出回答的信息
     * @param chatCompletionRequest
     * @return
     */
    ChatCompletionAsyncResponse completionsAsync(
            ChatCompletionRequest chatCompletionRequest)
    throws Exception;

    /**
     * 辅助接收流式输出的信息,将流式请求的信息转换为CompletableFuture类型
     * @param chatCompletionRequest
     * @return
     * @throws Exception
     */
    CompletableFuture<String> completions(
            ChatCompletionRequest chatCompletionRequest)
    throws InterruptedException;




}
