package com.hakutaku.chatglmsdk.session;

import com.hakutaku.chatglmsdk.IChatGlmApi;
import com.hakutaku.chatglmsdk.executor.BaseExecutor;
import com.hakutaku.chatglmsdk.executor.glm.GLMExecutor;
import com.hakutaku.chatglmsdk.model.enums.Model;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.sse.EventSource;
import okhttp3.sse.EventSources;

import java.util.HashMap;

@Slf4j
@NoArgsConstructor
@AllArgsConstructor
public class Configuration {
    /**
     * GLM配置类
     */
    private String apiSecretKey;

    @Getter
    @Setter
    private String glmApiHost = "https://open.bigmodel.cn";

    @Getter
    private String apiSecret;

    @Getter
    private String apiKey;

    public void setApiSecretKey(String apiSecretKey){
        this.apiSecretKey = apiSecretKey;
        //  GLM的apikey是XXXX.XXXX,需要分割
        String[] tmparray = apiSecretKey.split("\\.");
        if(tmparray.length != 2) throw new RuntimeException("apiKey不合法");
        this.apiKey = tmparray[0];
        this.apiSecret = tmparray[1];
    }

    @Getter
    @Setter
    private IChatGlmApi chatGlmApi;

    @Getter
    @Setter
    private OkHttpClient okHttpClient;

    //  客户端配置
    @Getter
    @Setter
    private HttpLoggingInterceptor.Level level = HttpLoggingInterceptor
            .Level.HEADERS;

    @Getter
    @Setter
    private long timeoutDdl = 30;

    @Setter
    @Getter
    private long writeDdl = 30;

    @Setter
    @Getter
    private long readDdl = 30;

    private HashMap<Model, BaseExecutor> executorMap;

    //  添加一些标识符
    public static final String SSE_CONTENT_TYPE = "text/event-stream";
    public static final String DEFAULT_USER_AGENT = "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)";
    public static final String APPLICATION_JSON = "application/json";
    public static final String JSON_CONTENT_TYPE = APPLICATION_JSON + "; charset=utf-8";

    //  初始化
    public EventSource.Factory createRequestFactory(){
        return EventSources.createFactory(okHttpClient);
    }

    //  初始化加入模型
    public HashMap<Model, BaseExecutor> initExecutorMap(){
        this.executorMap = new HashMap<>();
        BaseExecutor glmExecutor = new GLMExecutor(this);
        this.executorMap.put(Model.GLM4_FLASH,glmExecutor);
        this.executorMap.put(Model.GLM4_LONG,glmExecutor);
        this.executorMap.put(Model.GLM4_AIR,glmExecutor);
        this.executorMap.put(Model.GLM4_PLUS,glmExecutor);
        this.executorMap.put(Model.COGVIEW3_FLASH,glmExecutor);

        return this.executorMap;

    }

}
