package com.hakutaku.chatglmsdk.session;

public interface ChatGlmSessionFactory {

    ChatGlmSession chatGlmSession();
}
