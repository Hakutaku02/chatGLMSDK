package com.hakutaku.chatglmsdk.session.defaults;

import com.hakutaku.chatglmsdk.executor.BaseExecutor;
import com.hakutaku.chatglmsdk.model.ChatCompletionAsyncResponse;
import com.hakutaku.chatglmsdk.model.ChatCompletionRequest;
import com.hakutaku.chatglmsdk.model.enums.Model;
import com.hakutaku.chatglmsdk.session.ChatGlmSession;
import com.hakutaku.chatglmsdk.session.ChatGlmSessionFactory;
import com.hakutaku.chatglmsdk.session.Configuration;
import okhttp3.sse.EventSource;
import okhttp3.sse.EventSourceListener;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class DefaultChatGlmSession implements ChatGlmSession {

    private final Configuration configuration;
    private final Map<Model, BaseExecutor> executorGroup;

    public DefaultChatGlmSession(Configuration configuration,
                                 Map<Model, BaseExecutor> executorGroup){
        this.configuration = configuration;
        this.executorGroup = executorGroup;
    }

    @Override
    public EventSource completions(
            ChatCompletionRequest chatCompletionRequest,
            EventSourceListener eventSourceListener) throws Exception {
        BaseExecutor executor = executorGroup.get(chatCompletionRequest
                .getModel());
        if(null == executor) throw new RuntimeException(chatCompletionRequest.getModel()
                + " 模型执行器尚未实现！");
        return executor.completions(chatCompletionRequest,
                eventSourceListener);
    }

    @Override
    public ChatCompletionAsyncResponse completionsAsync(
            ChatCompletionRequest chatCompletionRequest)
            throws Exception {
        BaseExecutor executor = executorGroup.get(chatCompletionRequest.getModel());
        if (null == executor) throw new RuntimeException(chatCompletionRequest.getModel() + " 模型执行器尚未实现！");
        return executor.completionsAsync(chatCompletionRequest);

    }

    @Override
    public CompletableFuture<String> completions(ChatCompletionRequest chatCompletionRequest) throws InterruptedException {
        BaseExecutor executor = executorGroup.get(chatCompletionRequest.getModel());
        if(null ==  executor) throw new RuntimeException(chatCompletionRequest.getModel() + " 模型执行器尚未实现！");
        return executor.completions(chatCompletionRequest);
    }

    @Override
    public Configuration config() {
        return configuration;
    }
}
