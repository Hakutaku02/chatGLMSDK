package com.hakutaku.chatglmsdk.session.defaults;

import com.hakutaku.chatglmsdk.IChatGlmApi;
import com.hakutaku.chatglmsdk.executor.BaseExecutor;
import com.hakutaku.chatglmsdk.interceptor.ChatGlmInterceptor;
import com.hakutaku.chatglmsdk.model.enums.Model;
import com.hakutaku.chatglmsdk.session.ChatGlmSession;
import com.hakutaku.chatglmsdk.session.ChatGlmSessionFactory;
import com.hakutaku.chatglmsdk.session.Configuration;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class DefaultChatGlmSessionFactory implements ChatGlmSessionFactory {

    private final Configuration configuration;
    public DefaultChatGlmSessionFactory(Configuration configuration){
        this.configuration = configuration;
    }

    @Override
    public ChatGlmSession chatGlmSession() {
        //  配置日志
        HttpLoggingInterceptor loggingInterceptor = new
                HttpLoggingInterceptor();
        loggingInterceptor.setLevel(configuration.getLevel());

        //  开启客户端
        OkHttpClient okHttpClient = new OkHttpClient()
                .newBuilder()
                .addInterceptor(loggingInterceptor)
                .addInterceptor(new ChatGlmInterceptor(configuration))
                .connectTimeout(configuration.getTimeoutDdl(), TimeUnit.SECONDS)
                .writeTimeout(configuration.getWriteDdl(), TimeUnit.SECONDS)
                .readTimeout(configuration.getReadDdl(), TimeUnit.SECONDS)
                .build();

        configuration.setOkHttpClient(okHttpClient);

        //  创建API服务
        IChatGlmApi iChatGlmApi = new Retrofit.Builder()
                .baseUrl(configuration.getGlmApiHost())
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create())
                .build().create(IChatGlmApi.class);

        configuration.setChatGlmApi(iChatGlmApi);

        HashMap<Model, BaseExecutor> executorgroup = configuration.initExecutorMap();

        return new DefaultChatGlmSession(configuration,executorgroup);
    }
}
