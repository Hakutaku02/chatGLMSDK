package com.hakutaku.chatglmsdk.interceptor;

import com.hakutaku.chatglmsdk.session.Configuration;
import com.hakutaku.chatglmsdk.utils.BearerTokenUtils;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
@Slf4j
public class ChatGlmInterceptor implements Interceptor {

    //  使用JWT加密Token
    private final Configuration config;
    public ChatGlmInterceptor(Configuration config) {
        this.config = config;
    }

    @NotNull
    @Override
    public Response intercept(Chain chain) throws IOException {
        //  获取原始请求
        Request orginalRequest = chain.request();
        Request request = orginalRequest.newBuilder()
                .url(orginalRequest.url())
                .header("Authorization",
                        "Bearer "
                                + BearerTokenUtils
                                    .getToken(
                                            config.getApiKey(),
                                            config.getApiSecret()))
                .header("Content-type",Configuration.JSON_CONTENT_TYPE)
                .method(orginalRequest.method(),orginalRequest.body())
                .build();
        log.info("Request Headers: {}",request.headers());
        return chain.proceed(request);
    }
}
