/*
 * @Author: Hakutaku Hakutaku02@outlook.com
 * @Date: 2025-02-16 10:32:13
 * @LastEditors: Hakutaku Hakutaku02@outlook.com
 * @LastEditTime: 2025-02-17 11:03:07
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package com.hakutaku.chatglmsdk.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Slf4j
public class BearerTokenUtils {

    //  设置缓存过期时间
    private static final  long expireTime = 30 * 60 * 1000;

    //  建立缓存
    public static Cache<String, String> cache = CacheBuilder.newBuilder()
            .expireAfterWrite(expireTime - (60 * 1000L), TimeUnit.MILLISECONDS)
            .build();


    //  对APIKEY签名,别忘了被拆分成两部分的APIKEY
    public static String getToken(String apiKey, String apiSecret){
        // 缓存Token
        String token = cache.getIfPresent(apiKey);
        if (null != token) return token;

        // 创建Token
        Algorithm algorithm = Algorithm.HMAC256(apiSecret.getBytes(StandardCharsets.UTF_8));
        Map<String, Object> payload = new HashMap<>();
        payload.put("api_key", apiKey);
        payload.put("exp", System.currentTimeMillis() + expireTime);
        payload.put("timestamp", Calendar.getInstance().getTimeInMillis());
        Map<String, Object> headerClaims = new HashMap<>();
        headerClaims.put("alg", "HS256");
        headerClaims.put("sign_type", "SIGN");
        token = JWT.create().withPayload(payload).withHeader(headerClaims).sign(algorithm);
        cache.put(apiKey, token);
        return token;
    }

}
